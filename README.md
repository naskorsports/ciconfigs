Naskorsports CI configs
===

In the past, each of our web projects had their own,  
completely independent from one another.  

This led to the configs slowly diverging, causing unpleasant surprises and  
effort to keep them all in check.

The goal of this repo is to rectify this by serving as a central location from  
which standard job definitions or even whole CI configs can be included.

Since the `include` keyword for `.gitlab-ci.yml` doesn't support authentication,
this has to be public for now.
